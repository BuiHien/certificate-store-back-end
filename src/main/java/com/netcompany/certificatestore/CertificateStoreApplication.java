package com.netcompany.certificatestore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CertificateStoreApplication {

	public static void main(String[] args) {
		SpringApplication.run(CertificateStoreApplication.class, args);
	}

}
